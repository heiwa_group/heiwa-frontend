# Heiwa-frontend

A frontend for [Heiwa](https://gitlab.com/tomas.hz/heiwa). Unfinished, but being worked on.

Requires Python 3.10.

## Running

### Configuration

Copy the `config.example.json` file to `config.json`, and change the `SECRET_KEY`
value. This should ideally be very long sequences of ASCII, as well as non-ASCII
characters. For example, `*B8付Llbpふ7LとPšd3€a7!A様ůřShov&žt3Nm&9peIKig6J%でもそう繰り返すččšbyにSqMB5ěcøけば+いつまがkpP1`.

You also most likely want to change the `PUBLIC_API_URL` value, to a publicly
available URL of the backend, without a trailing slash. The default is fit only
for development environments.

If you wish to change any other config values, do so at this point.

### Running standalone

Change the `API_URL` config key to a full, accessible URL for the backend without
a trailing slash. The initial value is used for Docker containers.

Then, run these commands:

```bash
pip install -r requirements.txt  # Install Python dependencies
pip install gunicorn  # Install WSGI server
```

To start the server, run:

```bash
./run.sh
```

or bring your own web server that integrates the `heiwa_frontend:create_app()`
function.

### Running in Docker

If the backend is also in a container, connect both to the same networks:

```bash
docker network create heiwa-network  # Create virtual network, or use an existing one
docker network connect heiwa-network heiwa_heiwa_1  # Connect both containers, names may differ
docker network connect heiwa-network heiwa-frontend_heiwa_frontend_1
```

Otherwise, change the `API_URL` config key to a full, accessible URL for the
backend without a trailing slash.

Then, proceed with the standard way of running containers:

```bash
docker-compose up
```
