function generateFetchAuthorizationHeader() {
	return (
		"Bearer "
		+ (
			document.cookie
			.split("; ")
			.find(row => row.startsWith("token="))
			.split("=")[1]
		)
	);
}
