/* 
 * Sleeps for `ms` seconds, in an asynchronous manner.
 */
function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}

var receiverLookupRunning = false;
var newReceiverLookupWaiting = false;

/*
 * Sets the receiver ID to that of whoever the user clicks on.
 */
function setReceiver(event) {
	if (event.currentTarget.dataset.public_key == "") {
		alert(gettext("Sorry, this user cannot receive messages. They do not have a public key set."));

		return;
	}

	let receiverNameElement = document.getElementById("receiver-name");

	receiverNameElement.value = event.currentTarget.dataset.name;
	receiverNameElement.dataset.public_key = event.currentTarget.dataset.public_key;

	document.getElementById("receiver-search-view").replaceChildren();

	document.getElementById("receiver_id").value = event.currentTarget.dataset.id;
}

/*
 * Gets the list of possible receivers whose name begins with element ID
 * `receiver-name`'s value. As this uses regex to communicate with the API,
 * regex characters are automatically escaped.
 */
async function getReceiverList() {
	// Wait for previously running function to finish
	while (receiverLookupRunning) {
		newReceiverLookupWaiting = true;

		await sleep(200);
	}

	receiverLookupRunning = true;
	newReceiverLookupWaiting = false;

	let value = document.getElementById("receiver-name").value;

	// Escape regex characters
	// https://stackoverflow.com/a/3561711 - Thanks to bobince!
	value = value.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');

	let listElement = document.getElementById("receiver-search-view");

	if (value == "") {
		listElement.replaceChildren();
		receiverLookupRunning = false;

		return;
	}

	let list = null;

	const userListErrorMessage = gettext("Failed to get a list of the matching users. Please contact the administrator about this issue.");
	
	try {
		list = await (
			fetch(
				`${API_URL}/users`,
				{
					"method": "QUERY",
					"headers": {
						"Authorization": generateFetchAuthorizationHeader(),
						"Content-Type": "application/json"
					},
					"body": JSON.stringify({
						"limit": 50,
						"filter": {
							"$regex": {
								"name": `${value}.*`
							}
						}
					})
				}
			).
			then(response => {
				if (!response.ok) {
					alert(userListErrorMessage);
					
					throw new Error(
						`Failed to get a list of the matching users. The response code was ${response.code}.`
					);
				}

				return response.json();
			})
		);
	} catch (exception) {
		alert(userListErrorMessage);
		console.log(
			"User list fetch error: ",
			exception
		);

		return;
	}

	if (newReceiverLookupWaiting) {
		receiverLookupRunning = false;

		return;
	}

	listElement.replaceChildren();

	// Load elements first, then check if the curret user follows them
	for (const user of list) {
		const userMainElement = document.createElement("li");
		userMainElement.classList.add("receiver-search-item");
		userMainElement.id = `receiver-${user.id}`;
		userMainElement.dataset.id = user.id;
		userMainElement.dataset.name = user.name;
		userMainElement.dataset.public_key = ((user.public_key !== null) ? user.public_key : "");
		userMainElement.addEventListener("click", setReceiver)

		const userAvatarElement = document.createElement("img");
		userAvatarElement.src = `/users/${user.id}/avatar`;
		userAvatarElement.width = "25";
		userAvatarElement.height = "25";
		userAvatarElement.classList.add("receiver-search-avatar");

		userMainElement.appendChild(userAvatarElement);

		const userNameElement = document.createElement("span");
		userNameElement.classList.add("receiver-search-user-name");
		userNameElement.innerHTML = user.name;

		userMainElement.appendChild(userNameElement);

		listElement.appendChild(userMainElement);
	}

	for (const user of list) {
		if (newReceiverLookupWaiting) {
			receiverLookupRunning = false;
			
			return;
		}

		// Don't alert the user here, we can live without this and it could
		// make the experience worse if some minor error - like the rate limit
		// being exceeded - occurs

		try {
			let isFollowed = await (
				fetch(
					`${API_URL}/users/${user.id}/follow`,
					{
						"headers": {
							"Authorization": generateFetchAuthorizationHeader()
						}
					}
				).
				then(response => {
					if (!response.ok) {
						throw new Error(
							`Failed to check if user ID ${user.id} is followed. The HTTP code was ${response.code}.`
						);
					}
					
					return response.json();
				})
			);

			if (isFollowed) {
				let followedIcon = document.createElement("i");
				followedIcon.classList.add("fa-solid");
				followedIcon.classList.add("fa-user-check");

				document.getElementById(`receiver-${user.id}`).appendChild(followedIcon);
			}
		} catch (exception) {
			console.log(
				`Failed to get whether or not user ID ${user.id} is followed: `,
				exception
			);
		}
	}

	receiverLookupRunning = false;
}

/*
 * Generates a session key, IV and tag, encrypts the key and content, assigns
 * them to the respective inputs and submits the message form.
 */
function sendMessage() {
	let sessionKey = forge.random.getBytesSync(16);
	let iv = forge.random.getBytesSync(16);

	let utf8Encoder = new TextEncoder();

	let plaintextMessage = document.getElementById("content").value;

	let cipher = forge.cipher.createCipher("AES-CBC", sessionKey);
	cipher.start({iv: iv});

	cipher.update(
		forge.util.createBuffer(
			utf8Encoder.encode(plaintextMessage)
		)
	);

	cipher.finish();

	let encryptedContent = btoa(cipher.output.bytes());

	document.getElementById("iv").value = btoa(iv);
	document.getElementById("encrypted_content").value = encryptedContent;

	publicKey = forge.pki.publicKeyFromPem(
		"-----BEGIN PUBLIC KEY-----\n"
		+ document.getElementById("receiver-name").dataset.public_key
		+ "\n-----END PUBLIC KEY-----"
	)

	document.getElementById("encrypted_session_key").value = btoa(publicKey.encrypt(sessionKey));

	let md = forge.md.sha256.create();
	md.update(
		btoa(iv)
		+ encryptedContent
	);

	document.getElementById("tag").value = btoa(md.digest().bytes());

	document.getElementById("message-form").submit();
}
