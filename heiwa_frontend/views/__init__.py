from .file import file_blueprint
from .forum import forum_blueprint
from .message import message_blueprint
from .meta import meta_blueprint
from .account import account_blueprint
from .statistic import statistic_blueprint
from .tos import tos_blueprint
from .user import user_blueprint


__all__ = [
	"file_blueprint",
	"forum_blueprint",
	"message_blueprint",
	"meta_blueprint",
	"account_blueprint",
	"statistic_blueprint",
	"tos_blueprint",
	"user_blueprint"
]
__version__ = "0.0.21"
