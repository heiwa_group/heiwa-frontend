import functools
import typing

import flask

from .requests import request

__all__ = [
	"authentication_required",
	"full_view"
]


def authentication_required(
	function: typing.Callable
) -> typing.Callable:
	@functools.wraps(function)
	def decorator(*args, **kwargs) -> typing.Any:
		flask.g.token = flask.request.cookies.get("token")

		if flask.g.token is None:
			backend_config = request(
				flask.g.session,
				"get",
				"meta/config"
			).json()

			with flask.g.session.cache_disabled():
				guest_token = request(
					flask.g.session,
					"get",
					"guest/token",
					ignore_token=True
				).json()

			response = flask.make_response(
				flask.redirect(flask.request.path)
			)

			response.set_cookie(
				"token",
				guest_token,
				max_age=backend_config["guest_session_expires_after"]
			)

			return response

		return function(*args, **kwargs)
	return decorator


def full_view(
	anonymous: bool = False
) -> typing.Callable:
	def wrapper(function: typing.Callable) -> typing.Callable:
		@functools.wraps(function)
		def decorator(*args, **kwargs) -> typing.Any:
			flask.g.meta_info = request(
				flask.g.session,
				"get",
				"meta/info",
				ignore_token=True
			).json()

			flask.g.known_js_disabled = (
				True
				if flask.request.cookies.get("known_js_disabled") == "true"
				else False
			)

			if not anonymous:
				flask.g.user = request(
					flask.g.session,
					"get",
					"self"
				).json()

				flask.g.message_allowed_actions = request(
					flask.g.session,
					"get",
					"messages/allowed-actions"
				).json()

				if "view" in flask.g.message_allowed_actions:
					flask.g.unread_message_count = len(
						request(
							flask.g.session,
							"query",
							"messages",
							json={
								"limit": 501,
								"order": {
									"by": "creation_timestamp",
									"asc": False
								},
								"filter": {
									"$equals": {"is_read": False}
								}
							}
						).json()
					)

			return function(*args, **kwargs)
		return decorator
	return wrapper
