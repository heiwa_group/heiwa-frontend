from __future__ import annotations

import http.client
import typing

import flask
import flask_babel
import werkzeug.exceptions

__all__ = ["BackendException"]
__version__ = "0.0.3"


class BackendException(werkzeug.exceptions.HTTPException):
	TYPES = {
		"APIAuthorizationHeaderInvalid": flask_babel.lazy_gettext(
			"The backend has received an invalid request containing an invalid "
			"Authorization header, please contact the administrator about this issue."
		),
		"APIAuthorizationHeaderMissing": flask_babel.lazy_gettext(
			"The backend has received an invalid request containing no Authorization "
			"header, please contact the administrator about this issue."
		),
		"APIForumChildLevelLimitReached": flask_babel.lazy_gettext(
			"The forum is too many levels deep."
		),
		"APIForumNotFound": flask_babel.lazy_gettext(
			"The forum you've attempted to find doesn't exist."
		),
		"APIForumParentIsChild": flask_babel.lazy_gettext(
			"You've attempted to assign the forum as its own parent."
		),
		"APIForumPermissionsGroupNotFound": flask_babel.lazy_gettext(
			"Permissions don't exist for this forum and group."
		),
		"APIForumPermissionsGroupUnchanged": flask_babel.lazy_gettext(
			"No changes observed in the group permissions you've attempted to edit."
		),
		"APIForumPermissionsUserNotFound": flask_babel.lazy_gettext(
			"User permissions don't exist for this forum."
		),
		"APIForumPermissionsUserUnchanged": flask_babel.lazy_gettext(
			"No changes observed in the user permissions you've attempted to edit."
		),
		"APIForumSubscriptionAlreadyExists": flask_babel.lazy_gettext(
			"You've already subscribed to this forum."
		),
		"APIForumSubscriptionNotFound": flask_babel.lazy_gettext(
			"No subscription was found for this forum."
		),
		"APIForumUnchanged": flask_babel.lazy_gettext(
			"No changs observed in the forum you've attempted to edit."
		),
		"APIGroupCannotDeleteLastDefault": flask_babel.lazy_gettext(
			"The group you've attempted to delete is the last one default for all "
			"users."
		),
		"APIGroupCannotDeletePermissionsForLastDefault": flask_babel.lazy_gettext(
			"The group whose permissions you've attempted to delete is the last one "
			"default for all users."
		),
		"APIGroupCannotLeavePermissionNullForLastDefault": flask_babel.lazy_gettext(
			"Permissions have been left null for the last group default for all users."
		),
		"APIGroupNotFound": flask_babel.lazy_gettext(
			"The specified group was not found."
		),
		"APIGroupPermissionsNotFound": flask_babel.lazy_gettext(
			"No permissions found for this group."
		),
		"APIGroupPermissionsUnchanged": flask_babel.lazy_gettext(
			"No changes observed in the group permissions you've attempted to edit."
		),
		"APIGroupUnchanged": flask_babel.lazy_gettext(
			"No changes observed in the group you've attempted to edit."
		),
		"APIGuestSessionLimitReached": flask_babel.lazy_gettext(
			"Too many guests have signed up with the same IP as yours. "
			"Please try again in some time."
		),
		"APIJSONInvalid": flask_babel.lazy_gettext(
			"The backend has received a request with invalid JSON data. Please contact "
			"the administrator about this issue."
		),
		"APIJSONMissing": flask_babel.lazy_gettext(
			"The backend has received a request with missing JSON data, when there is "
			"supposed to be some. Please contact the administrator about this issue."
		),
		"APIAuthorizationJWTInvalid": flask_babel.lazy_gettext(
			"The access token your browser has presented is invalid. "
			"Please clear your cookies and try again."
		),
		"APIAuthorizationJWTInvalidClaims": flask_babel.lazy_gettext(
			"The access token your browser has presented contains invalid claims, "
			"meaning it's possibly expired. Please clear your cookies and try again."
		),
		"APIAuthorizationJWTSubjectNotFound": flask_babel.lazy_gettext(
			"The user account your access token represents seems to be deleted or "
			"inexistent. If you're sure this is a mistake, please contact the "
			"administrator. Otherwise, please make a new account and try again."
		),
		"APIMessageCannotChangeIsReadOfSent": flask_babel.lazy_gettext(
			"The message you've attempted to mark as read / unread belongs to "
			"a different receiver."
		),
		"APIMessageCannotSendToSelf": flask_babel.lazy_gettext(
			"The message you've attempted to send is addressed to yourself."
		),
		"APIMessageNotFound": flask_babel.lazy_gettext(
			"The specified message was not found."
		),
		"APIMessageReceiverBlockedSender": flask_babel.lazy_gettext(
			"This message's receiver has blocked you."
		),
		"APIMessageUnchanged": flask_babel.lazy_gettext(
			"No changes observed in the message you've attempted to edit."
		),
		"APINoPermission": flask_babel.lazy_gettext(
			"You don't have permission to perform this action."
		),
		"APINotificationNotFound": flask_babel.lazy_gettext(
			"The specified notification was not found."
		),
		"APIOpenIDAuthenticationFailed": flask_babel.lazy_gettext(
			"The authentication has failed."
		),
		"APIOpenIDNonceInvalid": flask_babel.lazy_gettext(
			"The nonce presented within your authentication request is invalid."
		),
		"APIOpenIDServiceNotFound": flask_babel.lazy_gettext(
			"No OpenID service with this name was found."
		),
		"APIOpenIDStateInvalid": flask_babel.lazy_gettext(
			"The state presented within your authentication request is invalid."
		),
		"APIPostNotFound": flask_babel.lazy_gettext(
			"The specified post was not found."
		),
		"APIPostUnchanged": flask_babel.lazy_gettext(
			"No changes observed in the post you've attempted to edit."
		),
		"APIPostVoteNotFound": flask_babel.lazy_gettext(
			"No vote found for this post."
		),
		"APIPostVoteUnchanged": flask_babel.lazy_gettext(
			"No changes observed in the vote you've attempted to edit."
		),
		"APIRateLimitExceeded": flask_babel.lazy_gettext(
			"You've exceeded the rate limit."
		),
		"APIThreadLocked": flask_babel.lazy_gettext(
			"This thread is locked."
		),
		"APIThreadNotFound": flask_babel.lazy_gettext(
			"The specified thread was not found."
		),
		"APIThreadSubscriptionAlreadyExists": flask_babel.lazy_gettext(
			"You've already subscribed to this thread."
		),
		"APIThreadSubscriptionNotFound": flask_babel.lazy_gettext(
			"No subscription found for this thread."
		),
		"APIThreadUnchanged": flask_babel.lazy_gettext(
			"No changes observed in the thread you've attempted to edit."
		),
		"APIThreadVoteNotFound": flask_babel.lazy_gettext(
			"No vote found for this thread."
		),
		"APIThreadVoteUnchanged": flask_babel.lazy_gettext(
			"No changes observed in the vote you've attempted to edit."
		),
		"APIUserAvatarInvalid": flask_babel.lazy_gettext(
			"The avatar is invalid."
		),
		"APIUserAvatarNotAllowedType": flask_babel.lazy_gettext(
			"The avatar is of a forbidden type."
		),
		"APIUserAvatarTooLarge": flask_babel.lazy_gettext(
			"The avatar is too large."
		),
		"APIUserBanAlreadyExpired": flask_babel.lazy_gettext(
			"The ban you've attempted to create would have already expired."
		),
		"APIUserBanNotFound": flask_babel.lazy_gettext(
			"No ban found for this user."
		),
		"APIUserBanUnchanged": flask_babel.lazy_gettext(
			"No changes observed in the ban you've attempted to edit."
		),
		"APIUserBanned": flask_babel.lazy_gettext(
			"You've been banned."
		),
		"APIUserBlockAlreadyExists": flask_babel.lazy_gettext(
			"This user has already been blocked."
		),
		"APIUserBlockNotFound": flask_babel.lazy_gettext(
			"No block found for this user."
		),
		"APIUserCannotRemoveLastDefaultGroup": flask_babel.lazy_gettext(
			"The group you've attempted to remove from this user is the last one "
			"default for all users."
		),
		"APIUserFollowAlreadyExists": flask_babel.lazy_gettext(
			"You've already followed this user."
		),
		"APIUserGroupAlreadyAdded": flask_babel.lazy_gettext(
			"This user already has this group."
		),
		"APIUserGroupNotAdded": flask_babel.lazy_gettext(
			"This user has never had this group."
		),
		"APIUserNotFound": flask_babel.lazy_gettext(
			"The specified user was not found."
		),
		"APIUserPermissionsUnchanged": flask_babel.lazy_gettext(
			"No changes observed in the permissions you've attempted to edit."
		),
		"APIUserUnchanged": flask_babel.lazy_gettext(
			"No changes observed in the user you've attempted to edit."
		)
	}

	def __init__(
		self: BackendException,
		type_: str,
		details: typing.Union[
			None,
			str,
			int,
			typing.Dict[
				str,
				typing.Union[
					typing.Dict,
					typing.Any
				]
			],
		] = None,
		code: int = http.client.INTERNAL_SERVER_ERROR,
		**kwargs
	) -> None:
		if type_ not in self.TYPES:
			self.description = flask_babel.lazy_gettext(
				"An unknown error with no description has occurred, please contact "
				"the administrator about this issue."
			)
		else:
			self.description = self.TYPES[type_]

		if str(code).startswith("5"):
			flask.current_app.logger.error(
				"The backend API has returned a %s status code. This very likely "
				"means an unforeseen exception occurred, or the server is under "
				"excessive load.",
				code
			)

		self.details = details
		self.code = code

		werkzeug.exceptions.HTTPException.__init__(self, **kwargs)
